package com.mobilapp.covid19

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobilapp.covid19.stateData.StateData
import com.mobilapp.covid19.stateData.StateDataAdapter
import com.mobilapp.covid19.stateData.StateDataPresenter
import com.mobilapp.covid19.stateData.StateDataView

class MainActivity : AppCompatActivity(), StateDataView {

    lateinit var rvCovidData: RecyclerView
    lateinit var stateDataPresenter: StateDataPresenter
    lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvCovidData = findViewById(R.id.rv_covid_data)
        progressBar = findViewById(R.id.progress_bar)
        rvCovidData.layoutManager = LinearLayoutManager(this)

        stateDataPresenter = StateDataPresenter(this)
        stateDataPresenter.loadCovidAPI()
    }

    override fun setStateDataOnSuccess(listOfStateData: List<StateData>) {
        rvCovidData.visibility = View.VISIBLE
        rvCovidData.adapter = StateDataAdapter(this, listOfStateData)
    }

    override fun showErrorMessage(message: String) {
        rvCovidData.visibility = View.GONE
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

}
