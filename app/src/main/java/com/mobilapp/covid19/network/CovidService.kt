package com.mobilapp.covid19.network

import com.mobilapp.covid19.stateData.CovidResponse
import retrofit2.Call
import retrofit2.http.GET

interface APIInterfaceCovid {

    @GET("/data.json")
    fun getStateWiseData(): Call<CovidResponse>
}