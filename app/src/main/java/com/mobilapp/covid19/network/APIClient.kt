package com.mobilapp.covid19.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class APIClient {
    companion object {
        fun getApiClient(): Retrofit {
            return Retrofit.Builder()
                .baseUrl("https://api.covid19india.org")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        }

        public fun covidAPIInterface(): APIInterfaceCovid {
            return getApiClient().create(APIInterfaceCovid::class.java)
        }
    }
}