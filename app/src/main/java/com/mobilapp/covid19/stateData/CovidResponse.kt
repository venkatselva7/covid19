package com.mobilapp.covid19.stateData

import com.google.gson.annotations.SerializedName

class CovidResponse {

    @SerializedName("statewise")
    var statewise: List<StateData>? = null
}

class StateData {

    @SerializedName("active")
    var active: String? = null
    @SerializedName("confirmed")
    var confirmed: String? = null
    @SerializedName("deaths")
    var deaths: String? = null
    @SerializedName("delta")
    var delta: StateDelta? = null
    @SerializedName("lastupdatedtime")
    var lastupdatedtime: String? = null
    @SerializedName("recovered")
    var recovered: String? = null
    @SerializedName("state")
    var state: String? = null
}

class StateDelta {
    @SerializedName("active")
    var active: Int? = null
    @SerializedName("confirmed")
    var confirmed: Int? = null
    @SerializedName("deaths")
    var deaths: Int? = null
    @SerializedName("recovered")
    var recovered: Int? = null
}