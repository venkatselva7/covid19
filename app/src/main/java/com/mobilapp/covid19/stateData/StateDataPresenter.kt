package com.mobilapp.covid19.stateData

import android.util.Log
import com.mobilapp.covid19.network.APIClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StateDataPresenter(val stateDataView: StateDataView) {

    fun loadCovidAPI() {
        stateDataView.showProgressBar()
        val call = APIClient.covidAPIInterface().getStateWiseData()
        call.enqueue(object : Callback<CovidResponse?> {
            override fun onResponse(
                call: Call<CovidResponse?>,
                response: Response<CovidResponse?>
            ) {
                call.cancel()
                stateDataView.hideProgressBar()
                if (response.isSuccessful) {
                    response.body()?.statewise?.let {
                        if (it.isEmpty()) {
                            stateDataView.showErrorMessage("No data found. Try again later")
                        } else {
                            stateDataView.setStateDataOnSuccess(it)
                        }
                        return
                    }
                }
                stateDataView.showErrorMessage("Something went wrong, Try again later")
            }

            override fun onFailure(
                call: Call<CovidResponse?>,
                t: Throwable
            ) {
                call.cancel()
                stateDataView.hideProgressBar()
                Log.d("TAG", "Failed" + t.message.toString())
                stateDataView.showErrorMessage("Something went wrong, Try again later")
            }
        })
    }
}