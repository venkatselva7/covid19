package com.mobilapp.covid19.stateData

interface StateDataView {
    fun setStateDataOnSuccess(listOfStateData: List<StateData>)
    fun showErrorMessage(message: String)
    fun showProgressBar()
    fun hideProgressBar()
}