package com.mobilapp.covid19.stateData

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.mobilapp.covid19.R


class StateDataAdapter(
    val context: Context,
    val listOfStateData: List<StateData>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return StateViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_state, parent, false))
    }

    override fun getItemCount(): Int {
        return listOfStateData.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
      if(holder is StateViewHolder){
          val stateData=listOfStateData[position]
          stateData.state?.let {
              holder.tvStateName.text = it
          }
          stateData.confirmed?.let {
              holder.tvConfirmedCase.text = "$it Confirmed Case"
          }
          stateData.active?.let {
              holder.tvActiveCase.text = "$it Active Case"
          }
          stateData.deaths?.let {
              holder.tvDeathCase.text = "$it Death Case"
          }
      }
    }

    class StateViewHolder(itemView: View) : ViewHolder(itemView) {
        var tvStateName: TextView = itemView.findViewById(R.id.tv_state_name)
        var tvConfirmedCase: TextView = itemView.findViewById(R.id.tv_confirmed_case)
        var tvActiveCase: TextView = itemView.findViewById(R.id.tv_active_case)
        var tvDeathCase: TextView = itemView.findViewById(R.id.tv_death_case)
    }
}
